<?php


namespace Mlh\AuthoringToolSdk;


use Mlh\AuthoringToolSdk\Exceptions\RequestDataInvalidException;

class SignatureGenerator implements \Mlh\AuthoringToolSdk\Interfaces\SignatureGenerator
{
    /**
     * @param int $integrationId
     * @param array $requestData - received from $requestBody['data']
     * @return string
     */
    public function generate(int $integrationId, array $requestData): string
    {
        if (!openssl_sign($this->getDataToVerify($integrationId, $requestData), $signature, $this->getKey(), OPENSSL_ALGO_SHA256)) {
            throw new \RuntimeException(sprintf('OpenSSL data signing failed with error: %s', openssl_error_string()));
        }

        return base64_encode($signature);
    }

    /**
     * @return resource
     */
    private function getKey()
    {
        $fp = fopen(__DIR__ . '/../keys/private.key', 'r');
        $privKey = fread($fp, 8192);
        fclose($fp);

        return openssl_pkey_get_private($privKey);
    }

    /**
     * @param int $integrationId
     * @param array $requestData
     * @return string
     */
    public function getDataToVerify(int $integrationId, array $requestData)
    {
        if (!isset($requestData['id'])) {
            throw new RequestDataInvalidException();
        }

        return $integrationId . $requestData['id'] . $integrationId;
    }
}
