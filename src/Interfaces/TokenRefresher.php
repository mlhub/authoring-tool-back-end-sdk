<?php

namespace Mlh\AuthoringToolSdk\Interfaces;

use Mlh\AuthoringToolSdk\Exceptions\RefreshTokenFailedException;

interface TokenRefresher
{
    /**
     * @throws RefreshTokenFailedException
     */
    public function refreshToken(): void;
}
