<?php


namespace Mlh\AuthoringToolSdk\Interfaces;


use Mlh\AuthoringToolSdk\Exceptions\RequestDataInvalidException;

interface SignatureGenerator
{
    /**
     * @param int $integrationId
     * @param array $requestData - received from $requestBody['data']
     * @return string
     */
    public function generate(int $integrationId, array $requestData): string;

    /**
     * @param int $integrationId
     * @param array $requestData
     * @return string
     */
    public function getDataToVerify(int $integrationId, array $requestData);
}
