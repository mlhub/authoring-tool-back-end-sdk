<?php


namespace Mlh\AuthoringToolSdk\Interfaces;


use Mlh\AuthoringToolSdk\Exceptions\IntegrationDataNotSetException;
use Mlh\AuthoringToolSdk\Exceptions\RefreshTokenFailedException;

interface UrlManager
{
    /**
     * @return string
     * @throws IntegrationDataNotSetException
     * @throws RefreshTokenFailedException
     */
    public function getCreateCourseUrl(): string;

    /**
     * @param int $courseId
     * @return string
     * @throws IntegrationDataNotSetException
     * @throws RefreshTokenFailedException
     */
    public function getCoursePlayerUrl(int $courseId): string;

    /**
     * @return string
     * @throws IntegrationDataNotSetException
     * @throws RefreshTokenFailedException
     */
    public function getCoursesListUrl(): string;

    /**
     * @param int $courseId
     * @return string
     * @throws IntegrationDataNotSetException
     * @throws RefreshTokenFailedException
     */
    public function getCourseBuilderUrl(int $courseId): string;
}
