<?php

namespace Mlh\AuthoringToolSdk\Interfaces;

use Carbon\Carbon;
use Mlh\AuthoringToolSdk\Exceptions\IntegrationDataNotSetException;

/**
 * Interface TokenStore
 * @package Mlh\AuthoringToolSdk\Interfaces
 */
interface IntegrationSettingsRepository
{
    /**
     * @return string
     * @throws IntegrationDataNotSetException
     */
    public function getToken(): string;

    /**
     * @return Carbon
     * @throws IntegrationDataNotSetException
     */
    public function getExpiresAt(): Carbon;

    /**
     * @return int
     * @throws IntegrationDataNotSetException
     */
    public function getIntegrationId(): int;

    /**
     * @return string
     * @throws IntegrationDataNotSetException
     */
    public function getAppUrl(): string;

    /**
     * @return string
     * @throws IntegrationDataNotSetException
     */
    public function getApiUrl(): string;

    /**
     * @param string $token
     * @param Carbon $expiresAt
     */
    public function updateToken(string $token, Carbon $expiresAt): void;

    /**
     * @return bool
     */
    public function integrationIsActive(): bool;

    /**
     * @return int|null
     */
    public function getUserId(): ?int;
}
