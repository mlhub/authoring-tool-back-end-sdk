<?php


namespace Mlh\AuthoringToolSdk\Interfaces;


interface SignatureVerifier
{
    /**
     * @param string $signature - received from $requestBody['signature']
     * @param array $requestData - received from $requestBody['data']
     * @return bool
     */
    public function verify(string $signature, array $requestData): bool;
}
