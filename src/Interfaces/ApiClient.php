<?php

namespace Mlh\AuthoringToolSdk\Interfaces;

use Psr\Http\Message\ResponseInterface;

interface ApiClient
{
    /**
     * @param int $courseId
     * @return ResponseInterface
     */
    public function deleteCourse(int $courseId): ResponseInterface;
}
