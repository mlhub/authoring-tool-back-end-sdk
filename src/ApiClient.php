<?php


namespace Mlh\AuthoringToolSdk;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\Str;
use Mlh\AuthoringToolSdk\Exceptions\RefreshTokenFailedException;
use Mlh\AuthoringToolSdk\Interfaces\IntegrationSettingsRepository;
use Psr\Http\Message\ResponseInterface;

class ApiClient implements Interfaces\ApiClient
{
    /**
     * @var IntegrationSettingsRepository
     */
    private $integrationSettingsRepository;
    /**
     * @var string
     */
    private $apiUrl;
    /**
     * @var string
     */
    private $token;
    /**
     * @var Carbon
     */
    private $expiresAt;
    /**
     * @var TokenRefresher
     */
    private $tokenRefresher;
    /**
     * @var ClientInterface
     */
    private $apiClient;

    /**
     * UrlManager constructor.
     * @param IntegrationSettingsRepository $integrationSettingsRepository
     * @param TokenRefresher $tokenRefresher
     * @param Client $apiClient
     * @throws Exceptions\IntegrationDataNotSetException
     * @throws RefreshTokenFailedException
     */
    public function __construct(IntegrationSettingsRepository $integrationSettingsRepository, TokenRefresher $tokenRefresher, Client $apiClient)
    {
        $this->integrationSettingsRepository = $integrationSettingsRepository;
        $this->tokenRefresher = $tokenRefresher;

        $this->apiUrl = $this->integrationSettingsRepository->getApiUrl() ?? 'https://share.mylearninghub.com';

        if ($this->integrationSettingsRepository->integrationIsActive()) {
            $this->setToken();
        }
        $this->apiClient = $apiClient;
    }

    /**
     *
     * @throws RefreshTokenFailedException
     * @throws Exceptions\IntegrationDataNotSetException
     */
    private function setToken(): void
    {
        $this->token = $this->integrationSettingsRepository->getToken();
        $this->expiresAt = $this->integrationSettingsRepository->getExpiresAt();

        if ($this->expiresAt <= Carbon::now()->addHour()) {
            $this->tokenRefresher->refreshToken();
        }
    }

    /**
     * @param int $courseId
     * @return ResponseInterface
     */
    public function deleteCourse(int $courseId): ResponseInterface
    {
        return $this->sendDeleteRequest('/api/courses/' . $courseId);
    }

    /**
     * @param string $url
     * @return ResponseInterface
     */
    private function sendDeleteRequest(string $url)
    {
        return $this->apiClient->delete(
            $this->apiUrl . Str::start($url, '/'),
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->token,
                    'Accept' => 'application/json',
                ]
            ]
        );
    }
}
