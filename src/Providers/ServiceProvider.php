<?php


namespace Mlh\AuthoringToolSdk\Providers;

use Mlh\AuthoringToolSdk\Interfaces\ApiClient;
use Mlh\AuthoringToolSdk\Interfaces\SignatureVerifier;
use Mlh\AuthoringToolSdk\Interfaces\TokenRefresher;
use Mlh\AuthoringToolSdk\Interfaces\UrlManager;
use Mlh\AuthoringToolSdk\Interfaces\SignatureGenerator;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     *
     */
    public function register()
    {
        parent::register();

        $this->registerInDIContainer();
    }

    /**
     *
     */
    private function registerInDIContainer(): void
    {
        $this->app->bind(ApiClient::class, \Mlh\AuthoringToolSdk\ApiClient::class);
        $this->app->bind(SignatureGenerator::class, \Mlh\AuthoringToolSdk\SignatureGenerator::class);
        $this->app->bind(SignatureVerifier::class, \Mlh\AuthoringToolSdk\SignatureVerifier::class);
        $this->app->bind(TokenRefresher::class, \Mlh\AuthoringToolSdk\TokenRefresher::class);
        $this->app->bind(UrlManager::class, \Mlh\AuthoringToolSdk\UrlManager::class);
    }
}
