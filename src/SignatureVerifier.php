<?php

namespace Mlh\AuthoringToolSdk;

use Mlh\AuthoringToolSdk\Interfaces\IntegrationSettingsRepository;
use Mlh\AuthoringToolSdk\Interfaces\SignatureGenerator;
use RuntimeException;

class SignatureVerifier implements Interfaces\SignatureVerifier
{
    /**
     * @var int
     */
    private $integrationId;
    /**
     * @var IntegrationSettingsRepository
     */
    private $integrationSettingsRepository;
    /**
     * @var SignatureGenerator
     */
    private $signatureGenerator;

    /**
     * SignatureVerifier constructor.
     * @param IntegrationSettingsRepository $integrationSettingsRepository
     * @param SignatureGenerator $signatureGenerator
     * @throws Exceptions\IntegrationDataNotSetException
     */
    public function __construct(IntegrationSettingsRepository $integrationSettingsRepository, SignatureGenerator $signatureGenerator)
    {
        $this->integrationId = $integrationSettingsRepository->getIntegrationId();
        $this->integrationSettingsRepository = $integrationSettingsRepository;
        $this->signatureGenerator = $signatureGenerator;
    }

    /**
     * @param string $signature - received from $requestBody['signature']
     * @param array $requestData - received from $requestBody['data']
     * @return bool
     */
    public function verify(string $signature, array $requestData): bool
    {
        $dataToVerify = $this->signatureGenerator->getDataToVerify($this->integrationId, $requestData);
        $result = openssl_verify($dataToVerify, base64_decode($signature), $this->getKey(), OPENSSL_ALGO_SHA256);

        if ($result == 1) {
            return true;
        }

        if ($result === 0) {
            return false;
        }

        throw new RuntimeException('An error occurred while verifying the signature.');
    }

    /**
     * @return resource
     */
    private function getKey()
    {
        $fp = fopen(__DIR__ . '/../keys/public.key', 'r');
        $key = fread($fp, 8192);
        fclose($fp);

        return openssl_pkey_get_public($key);
    }
}
