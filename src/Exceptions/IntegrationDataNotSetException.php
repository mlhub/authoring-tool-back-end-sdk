<?php


namespace Mlh\AuthoringToolSdk\Exceptions;

use Exception;

class IntegrationDataNotSetException extends Exception
{
}