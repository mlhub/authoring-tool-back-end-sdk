<?php


namespace Mlh\AuthoringToolSdk\Exceptions;


class RequestDataInvalidException extends \InvalidArgumentException
{
    /**
     * RequestDataInvalidException constructor.
     */
    public function __construct()
    {
        parent::__construct('given request data should contain field "id"');
    }
}
