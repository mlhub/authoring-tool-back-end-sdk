<?php

namespace Mlh\AuthoringToolSdk;


use Carbon\Carbon;
use Illuminate\Support\Str;
use Mlh\AuthoringToolSdk\Exceptions\RefreshTokenFailedException;
use Mlh\AuthoringToolSdk\Interfaces\IntegrationSettingsRepository;

/**
 * Class UrlManager
 * @package Mlh\AuthoringToolSdk
 */
class UrlManager implements Interfaces\UrlManager
{
    /**
     * @var IntegrationSettingsRepository
     */
    private $integrationSettingsRepository;
    /**
     * @var string
     */
    private $appUrl;
    /**
     * @var string
     */
    private $apiUrl;
    /**
     * @var string
     */
    private $token;
    /**
     * @var Carbon
     */
    private $expiresAt;
    /**
     * @var TokenRefresher
     */
    private $tokenRefresher;
    /**
     * @var int|string|null
     */
    private $userId;

    /**
     * UrlManager constructor.
     * @param IntegrationSettingsRepository $integrationSettingsRepository
     * @param TokenRefresher $tokenRefresher
     * @throws Exceptions\IntegrationDataNotSetException
     */
    public function __construct(IntegrationSettingsRepository $integrationSettingsRepository, TokenRefresher $tokenRefresher)
    {
        $this->integrationSettingsRepository = $integrationSettingsRepository;
        $this->tokenRefresher = $tokenRefresher;

        $this->appUrl = $this->integrationSettingsRepository->getAppUrl() ?? 'https://share.mylearninghub.com';
        $this->apiUrl = $this->integrationSettingsRepository->getApiUrl() ?? 'https://share.mylearninghub.com';
        $this->userId = $this->integrationSettingsRepository->getUserId();
    }

    /**
     * @param int|null $courseId
     * @return string
     */
    public function getCourseBuilderUrl(int $courseId = null): string
    {
        return $courseId ? $this->getCourseBuilderUrlForCourse($courseId) : $this->getCreateCourseUrl();
    }

    /**
     * @param int $courseId
     * @return string
     */
    public function getCourseBuilderUrlForCourse(int $courseId): string
    {
        return $this->buildUrl('/courseBuilder/' . $courseId);
    }

    /**
     * @return string
     * @throws Exceptions\IntegrationDataNotSetException
     * @throws RefreshTokenFailedException
     */
    public function getCreateCourseUrl(): string
    {
        return $this->buildUrl('/createCourse');
    }

    /**
     * @return string
     * @throws Exceptions\IntegrationDataNotSetException
     * @throws RefreshTokenFailedException
     */
    public function getCoursesListUrl(): string
    {
        return $this->buildUrl('/coursesList');
    }

    /**
     * @param int $courseId
     * @return string
     * @throws Exceptions\IntegrationDataNotSetException
     * @throws RefreshTokenFailedException
     */
    public function getCoursePlayerUrl(int $courseId): string
    {
        return $this->buildUrl('/coursePlayer/' . $courseId);
    }

    /**
     * @deprecated use ApiClient class for making api requests
     * To delete this course you should send DELETE request on given url
     *
     * @param int $courseId
     * @return string
     * @throws RefreshTokenFailedException
     * @throws Exceptions\IntegrationDataNotSetException
     */
    public function getDeleteCourseUrl(int $courseId): string
    {
        $this->refreshToken();

        return $this->apiUrl . '/api/courses' . $courseId;
    }

    /**
     * @param string $path
     * @param string|null $baseUrl
     * @return string
     * @throws Exceptions\IntegrationDataNotSetException
     * @throws RefreshTokenFailedException
     */
    private function buildUrl(string $path, string $baseUrl = null): string
    {
        $this->refreshToken();

        $url = $baseUrl ? $baseUrl . '?' : $this->appUrl . Str::start($path, '/') . '?';
        $url .= 'token=' . $this->token;
        $url .= '&expiresIn=' . Carbon::now()->diffInSeconds($this->expiresAt);
        $url .= '&integration_type=api';
        $url .= '&integration_user_id=' . $this->userId;
        $url .= '&v=' . time();

        return $url;
    }

    /**
     * @throws RefreshTokenFailedException
     * @throws Exceptions\IntegrationDataNotSetException
     */
    private function refreshToken(): void
    {
        if ($this->integrationSettingsRepository->getExpiresAt() <= Carbon::now()->addHour()) {
            $this->tokenRefresher->refreshToken();
        }

        $this->expiresAt = $this->integrationSettingsRepository->getExpiresAt();
        $this->token = $this->integrationSettingsRepository->getToken();
    }
}
