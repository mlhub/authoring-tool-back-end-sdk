<?php

namespace Mlh\AuthoringToolSdk;

use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Mlh\AuthoringToolSdk\Exceptions\RefreshTokenFailedException;
use Mlh\AuthoringToolSdk\Interfaces\IntegrationSettingsRepository;

class TokenRefresher implements \Mlh\AuthoringToolSdk\Interfaces\TokenRefresher
{
    /**
     * @var IntegrationSettingsRepository
     */
    private $integrationSettingsRepository;
    /**
     * @var ClientInterface
     */
    private $httpClient;

    /**
     * TokenRefresher constructor.
     * @param IntegrationSettingsRepository $integrationSettingsRepository
     * @param Client $httpClient
     */
    public function __construct(IntegrationSettingsRepository $integrationSettingsRepository, Client $httpClient)
    {
        $this->integrationSettingsRepository = $integrationSettingsRepository;
        $this->httpClient = $httpClient;
    }

    /**
     * @throws RefreshTokenFailedException
     */
    public function refreshToken(): void
    {
        try {
            $this->refreshTokenWithApi();
        } catch (Exception $e) {
            throw new RefreshTokenFailedException($e->getMessage());
        }
    }

    /**
     * @throws Exceptions\IntegrationDataNotSetException
     * @throws Exception
     */
    private function refreshTokenWithApi()
    {
        $res = $this->httpClient->post($this->integrationSettingsRepository->getApiUrl(). '/api/api_integrations/refresh_token', [
            'form_params' => [
                'integration_id' => $this->integrationSettingsRepository->getIntegrationId(),
                'token' => $this->integrationSettingsRepository->getToken(),
            ],
        ]);

        if ($res->getStatusCode() !== 200) {
            throw new Exception('Refresh token failed with code: ' . $res->getStatusCode() . ' Reason: ' . $res->getReasonPhrase());
        }

        $result = $res->getBody()->getContents();
        $result = json_decode($result, true)['data'];

        $this->integrationSettingsRepository->updateToken($result['token'], Carbon::parse($result['expires_at']));
    }
}
