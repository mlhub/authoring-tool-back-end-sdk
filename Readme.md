# Installation

## Install package with composer
`composer require mlh/authoring_tool_sdk`

## Implement Interface IntegrationSettingsRepository
```php
<?php

namespace Mlh\AuthoringToolSdk;

use Carbon\Carbon;

class IntegrationSettingsRepository implements \Mlh\AuthoringToolSdk\Interfaces\IntegrationSettingsRepository
{
    /**
     * @return string
     */
    public function getToken(): string
    {
        return 'stored in your app integration token';
    }

    /**
     * @return Carbon
     */
    public function getExpiresAt(): Carbon
    {
        return 'stored in your app integration token expires at';
    }

    /**
     * @return int
     */
    public function getIntegrationId(): int
    {
        return 'stored in your app integration id';
    }

    /**
     * @return string
     */
    public function getAppUrl(): string
    {
        return 'https://authroingtool.mylearninghub.com';
    }

    /**
     * @return string
     */
    public function getApiUrl(): string
    {
        return 'https://authroingtool.mylearninghub.com:8000';
    }

    /**
     * @param string $token
     * @param Carbon $expiresAt
     */
    public function updateToken(string $token, Carbon $expiresAt): void
    {
        // some implementation of storing new credentials somewhere in your app
    }

    /**
     * @return bool
     */
    public function integrationIsActive(): bool
    {
        return true;
    }
}
```

## Register your implementation of IntegrationSettingsRepository in DI container of your app
```php
// laravel example
app()->bind(\Mlh\AuthoringToolSdk\Interfaces\IntegrationSettingsRepository::class, \YourApp\IntegrationSettingsRepositoryImplementation::class);
```

## (Not laravel only) Register all classes interfaces bindings in DI container of your app
```php
app()->bind(\Mlh\AuthoringToolSdk\Interfaces\ApiClient::class, \Mlh\AuthoringToolSdk\ApiClient::class);
app()->bind(\Mlh\AuthoringToolSdk\Interfaces\SignatureVerifier::class, \Mlh\AuthoringToolSdk\SignatureVerifier::class);
app()->bind(\Mlh\AuthoringToolSdk\Interfaces\TokenRefresher::class, \Mlh\AuthoringToolSdk\TokenRefresher::class);
app()->bind(\Mlh\AuthoringToolSdk\Interfaces\UrlManager::class, \Mlh\AuthoringToolSdk\UrlManager::class);
```

# Basic Usage

## Display create course form


```php
<?php

use \Mlh\AuthoringToolSdk\Interfaces\UrlManager;

class SomeController 
{
    /**
     * @var UrlManager 
     */
    private $urlManager;
    
    /**
     * SomeController constructor.
     * @param UrlManager $urlManager
     */
    public function __construct(UrlManager $urlManager) 
    {
        $this->urlManager = $urlManager;
    }
    
    /**
     * @return string
     */
    public function createCourseIframe(): string 
    {
        $iframeUrl = $this->urlManager->getCreateCourseUrl();
        return "<iframe allow='autoplay; fullscreen; microphone; camera' allowFullScreen webkitAllowFullScreen src='$iframeUrl'></iframe>";
    }
}
```

## Display course builder of course

```php
<?php

use \Mlh\AuthoringToolSdk\Interfaces\UrlManager;

class SomeController 
{
    /**
     * @var UrlManager 
     */
    private $urlManager;
    
    /**
     * SomeController constructor.
     * @param UrlManager $urlManager
     */
    public function __construct(UrlManager $urlManager) 
    {
        $this->urlManager = $urlManager;
    }
    
    /**
     * @param int $courseId
     * @return string
     */
    public function courseBuilderIframe(int $courseId): string 
    {
        $iframeUrl = $this->urlManager->getCourseBuilderUrl($courseId);
        return "<iframe allow='autoplay; fullscreen; microphone; camera' allowFullScreen webkitAllowFullScreen src='$iframeUrl'></iframe>";
    }
}
```

## Display course player for course

```php
<?php

use \Mlh\AuthoringToolSdk\Interfaces\UrlManager;

class SomeController 
{
    /**
     * @var UrlManager 
     */
    private $urlManager;
    
    /**
     * SomeController constructor.
     * @param UrlManager $urlManager
     */
    public function __construct(UrlManager $urlManager) 
    {
        $this->urlManager = $urlManager;
    }
    
    /**
     * @param int $courseId
     * @return string
     */
    public function coursePlayerIframe(int $courseId): string 
    {
        $iframeUrl = $this->urlManager->getCoursePlayerUrl($courseId);
        return "<iframe allow='autoplay; fullscreen; microphone; camera' allowFullScreen webkitAllowFullScreen src='$iframeUrl'></iframe>";
    }
}
```

## Delete course
```php
<?php 

use \Mlh\AuthoringToolSdk\Interfaces\ApiClient;

class SomeClassInYourApp 
{
    // ...
    
    /**
     * @var ApiClient
     */
    private $apiClient;
    
    /**
     * SomeClassInYourApp constructor.
     * @param ApiClient $apiClient
     */
    public function __construct(ApiClient $apiClient) 
    {
        $this->apiClient = $apiClient;
    }
    
    /**
     * @param int $courseId
     */
    public function deleteCourse(int $courseId): void 
    {
        $this->apiClient->deleteCourse($courseId);
    }
    
    //...
}
```

# Webhooks

## Route requirements
1. Route should accept POST method with json data
2. Route should answer "200" if all is ok
3. Route should answer "403 Signature invalid" if signature validation failed

## Webhook data
It is a json object with following keys:  
* `data` - where all the course data contains
* `signature` - verification string to check that request was sent from our API
Example of webhook data:
```json
{
  "data": {
    "id": 124,
    "title": "course title",
    "theme_id": 8,
    "user_id": 1234455,
    "user": {
      "id": 1234455,
      "name": "John Doe",
      "email": "example@test.com"
    }
  },
  "signature": "62347gfhdusy78fduyhe8hkjfdshouy"
}
```

## Signature verification
```php
<?php

use \Mlh\AuthoringToolSdk\Interfaces\SignatureVerifier;
use \Illuminate\Http\Request;

class SomeController 
{
    /**
     * @var SignatureVerifier 
     */
    private $signatureVerifier;
    
    /**
     * SomeController constructor.
     * @param SignatureVerifier $signatureVerifier
     */
    public function __construct(SignatureVerifier $signatureVerifier) 
    {
        $this->signatureVerifier = $signatureVerifier;
    }
    
    /**
     * @param int $courseId
     */
    public function courseCreatedWebhook(Request $request): string 
    {
        $signatureIsValid = $this->signatureVerifier->verify($request->input('signature'), $request->input('data'));
        
        if (!$signatureIsValid) {
            throw new HttpException('Signature invalid', 403);
        }
        
        // some code for handling webhook
        
        // return success response with code 200
        return response()->json([], 200);
    }
}
```
