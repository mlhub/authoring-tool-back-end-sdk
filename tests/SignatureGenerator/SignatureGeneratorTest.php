<?php


namespace Mlh\AuthoringToolSdk\Tests\SignatureGenerator;


use Mlh\AuthoringToolSdk\Exceptions\RequestDataInvalidException;
use Mlh\AuthoringToolSdk\SignatureGenerator;
use PHPUnit\Framework\TestCase;

class SignatureGeneratorTest extends TestCase
{
    /**
     * @var \Mlh\AuthoringToolSdk\Interfaces\SignatureGenerator
     */
    private $generator;

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->generator = new SignatureGenerator();
    }

    /**
     *
     */
    public function testSignatureWithValidDataGenerated()
    {
        $data = [
            'id' => 1234,
        ];
        $integrationId = 2343545;

        $signature = $this->generator->generate($integrationId, $data);

        $this->assertNotNull($signature);
        $this->assertNotNull(json_encode($signature));
        $this->assertEquals(JSON_ERROR_NONE, json_last_error());
    }

    /**
     *
     */
    public function testThrowsErrorIfGivenDataIsInvalid()
    {
        $this->expectException(RequestDataInvalidException::class);

        $data = [];
        $integrationId = 2343545;

        $this->generator->generate($integrationId, $data);
    }
}
