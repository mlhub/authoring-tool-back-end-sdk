<?php


namespace Mlh\AuthoringToolSdk\Tests\SignatureVerifier;

use Illuminate\Support\Str;
use Mlh\AuthoringToolSdk\Exceptions\IntegrationDataNotSetException;
use Mlh\AuthoringToolSdk\Exceptions\RequestDataInvalidException;
use Mlh\AuthoringToolSdk\SignatureGenerator;
use Mlh\AuthoringToolSdk\Interfaces\SignatureGenerator as SignatureGeneratorInterface;
use Mlh\AuthoringToolSdk\SignatureVerifier;
use Mlh\AuthoringToolSdk\Interfaces\SignatureVerifier as SignatureVerifierInterface;
use Mlh\AuthoringToolSdk\Tests\TestUtils\IntegrationSettingsRepository;
use Mlh\AuthoringToolSdk\Interfaces\IntegrationSettingsRepository as IntegrationSettingsRepositoryInterface;
use PHPUnit\Framework\TestCase;

class SignatureVerifierTest extends TestCase
{
    /**
     * @var IntegrationSettingsRepositoryInterface
     */
    private $repository;
    /**
     * @var SignatureVerifierInterface
     */
    private $verifier;
    /**
     * @var SignatureGeneratorInterface
     */
    private $generator;

    /**
     * @throws IntegrationDataNotSetException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = new IntegrationSettingsRepository();
        $this->generator = new SignatureGenerator();
        $this->verifier = new SignatureVerifier($this->repository, $this->generator);
    }

    /**
     * @throws IntegrationDataNotSetException
     */
    public function testSignatureValidReturnsTrue()
    {
        $data = [
            'id' => 1234,
            'test2' => 67889,
        ];
        $signature = $this->generator->generate($this->repository->getIntegrationId(), $data);

        $this->assertTrue($this->verifier->verify($signature, $data));
    }

    /**
     *
     */
    public function testSignatureInvalidReturnsFalse()
    {
        $data = [
            'id' => 1234,
            'test2' => 67889,
        ];
        $signature = Str::random();

        $this->assertFalse($this->verifier->verify($signature, $data));
    }

    /**
     *
     */
    public function testInvalidRequestDataThrowsError()
    {
        $this->expectException(RequestDataInvalidException::class);
        $data = [];
        $signature = Str::random();

        $this->verifier->verify($signature, $data);
    }
}
