<?php


namespace Mlh\AuthoringToolSdk\Tests\TestUtils;

use Carbon\Carbon;
use Mlh\AuthoringToolSdk\Interfaces\IntegrationSettingsRepository as SettingsRepositoryInterface;

class IntegrationSettingsRepository implements SettingsRepositoryInterface
{
    /**
     * @var string
     */
    private $token = 'valid token';
    /**
     * @var Carbon
     */
    private $expiresAt;
    /**
     * @var int
     */
    private $integrationId = 1234;
    /**
     * @var int|null
     */
    private $userId = 2344;

    /**
     * IntegrationSettingsRepository constructor.
     * @param string|null $token
     * @param Carbon|null $expiresAt
     * @param int|null $integrationId
     * @param int|null $userId
     */
    public function __construct(?string $token = null, ?Carbon $expiresAt = null, ?int $integrationId = null, ?int $userId = null)
    {
        $this->token = $token ?? $this->token;
        $this->expiresAt = $expiresAt ?? Carbon::now()->addWeek();
        $this->integrationId = $integrationId ?? $this->integrationId;
        $this->userId = $userId ?? $this->userId;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return Carbon
     */
    public function getExpiresAt(): Carbon
    {
        return $this->expiresAt;
    }

    /**
     * @return int
     */
    public function getIntegrationId(): int
    {
        return $this->integrationId;
    }

    /**
     * @return string
     */
    public function getAppUrl(): string
    {
        return 'http://example.com';
    }

    /**
     * @return string
     */
    public function getApiUrl(): string
    {
        return 'http://api.example.com';
    }

    /**
     * @param string $token
     * @param Carbon $expiresAt
     */
    public function updateToken(string $token, Carbon $expiresAt): void
    {
        $this->token = $token;
        $this->expiresAt = $expiresAt;
    }

    /**
     * @return bool
     */
    public function integrationIsActive(): bool
    {
        return true;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }
}
