<?php


namespace Mlh\AuthoringToolSdk\Tests;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Mlh\AuthoringToolSdk\Exceptions\IntegrationDataNotSetException;
use Mlh\AuthoringToolSdk\Exceptions\RefreshTokenFailedException;
use Mlh\AuthoringToolSdk\Tests\TestUtils\IntegrationSettingsRepository;
use Mlh\AuthoringToolSdk\Interfaces\IntegrationSettingsRepository as IntegrationSettingsRepositoryInterface;
use Mlh\AuthoringToolSdk\TokenRefresher;
use Mlh\AuthoringToolSdk\UrlManager;
use Mlh\AuthoringToolSdk\Interfaces\UrlManager as UrlManagerInterface;
use PHPUnit\Framework\TestCase;

class ValidCredentialsTest extends TestCase
{
    /**
     * @var IntegrationSettingsRepositoryInterface
     */
    private $settingsRepository;
    /**
     * @var UrlManagerInterface
     */
    private $urlManager;

    /**
     * @throws RefreshTokenFailedException
     * @throws IntegrationDataNotSetException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->settingsRepository = new IntegrationSettingsRepository();
        $this->urlManager = new UrlManager($this->settingsRepository, new TokenRefresher($this->settingsRepository, new Client()));
    }

    /**
     *
     * @throws IntegrationDataNotSetException
     */
    public function testGetCreateCourseUrl()
    {
        $result = $this->urlManager->getCreateCourseUrl();

        $this->checkUrlContainsAllRequiredParts($result);
    }

    /**
     *
     * @throws IntegrationDataNotSetException
     */
    public function testGetCoursesListUrl()
    {
        $result = $this->urlManager->getCoursesListUrl();

        $this->checkUrlContainsAllRequiredParts($result);
    }

    /**
     *
     * @throws IntegrationDataNotSetException
     */
    public function testGetCourseBuilderUrl()
    {
        $courseId = 122443;
        $result = $this->urlManager->getCourseBuilderUrl($courseId);

        $this->checkUrlContainsAllRequiredParts($result);
        $this->assertStringContainsString($courseId, $result);
    }

    /**
     * @param string $resultUrl
     * @throws IntegrationDataNotSetException
     */
    private function checkUrlContainsAllRequiredParts(string $resultUrl)
    {
        $this->assertStringContainsString($this->settingsRepository->getAppUrl(), $resultUrl);
        $this->assertStringContainsString('token=' . $this->settingsRepository->getToken(), $resultUrl);
        $this->assertStringContainsString('integration_type=api', $resultUrl);
        $this->assertStringContainsString('expiresIn=' . Carbon::now()->diffInSeconds($this->settingsRepository->getExpiresAt()), $resultUrl);
        $this->assertStringContainsString('integration_user_id=' . $this->settingsRepository->getUserId(), $resultUrl);
    }
}
