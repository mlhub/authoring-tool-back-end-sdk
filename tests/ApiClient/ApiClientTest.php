<?php


namespace Mlh\AuthoringToolSdk\Tests\ApiClient;


use GuzzleHttp\Client;
use Mlh\AuthoringToolSdk\ApiClient;
use Mlh\AuthoringToolSdk\Exceptions\IntegrationDataNotSetException;
use Mlh\AuthoringToolSdk\Exceptions\RefreshTokenFailedException;
use Mlh\AuthoringToolSdk\Tests\TestUtils\IntegrationSettingsRepository;
use Mlh\AuthoringToolSdk\TokenRefresher;
use Mockery;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

class ApiClientTest extends TestCase
{
    /**
     * @throws IntegrationDataNotSetException
     * @throws RefreshTokenFailedException
     */
    public function testDeleteCourse()
    {
        $courseId = 123;

        $tokenRefresherMock = Mockery::mock(TokenRefresher::class);
        $repository = new IntegrationSettingsRepository();

        $responseMock = Mockery::mock(ResponseInterface::class);
        $httpClientMock = Mockery::mock(Client::class);
        $httpClientMock->expects('delete')
            ->once()
            ->withArgs(function (string $url, array $options) use ($courseId, $repository) {
                $urlValid = $url === $repository->getApiUrl() . '/api/courses/' . $courseId;
                $authHeaderIsValid = $options['headers']['Authorization'] === 'Bearer ' . $repository->getToken();
                $acceptHeaderIsValid = $options['headers']['Accept'] === 'application/json';

                return $urlValid && $authHeaderIsValid && $acceptHeaderIsValid;
            })
            ->andReturn($responseMock);

        $apiClient = new ApiClient($repository, $tokenRefresherMock, $httpClientMock);

        $result = $apiClient->deleteCourse($courseId);

        $this->assertEquals($responseMock, $result);
    }
}
