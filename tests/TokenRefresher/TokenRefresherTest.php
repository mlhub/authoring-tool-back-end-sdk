<?php


namespace Mlh\AuthoringToolSdk\Tests\TokenRefresher;


use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Mlh\AuthoringToolSdk\Exceptions\RefreshTokenFailedException;
use Mlh\AuthoringToolSdk\Interfaces\IntegrationSettingsRepository;
use Mlh\AuthoringToolSdk\TokenRefresher;
use Mockery;
use PHPUnit\Framework\ExpectationFailedException;
use PHPUnit\Framework\TestCase;
use SebastianBergmann\RecursionContext\InvalidArgumentException;

class TokenRefresherTest extends TestCase
{
    /**
     * @throws RefreshTokenFailedException
     * @throws ExpectationFailedException
     * @throws InvalidArgumentException
     */
    public function testResponseOkSetNewDataToRepository()
    {
        $newToken = 'new token';
        $newExpiresAt = Carbon::now()->addWeek();

        $repository = Mockery::mock(IntegrationSettingsRepository::class);
        $repository->expects('updateToken')
            ->with(
                $newToken,
                Mockery::on(function (Carbon $argument) use ($newExpiresAt) {
                    return $argument->toString() === $newExpiresAt->toString();
                })
            )
            ->once();
        $repository->expects('getIntegrationId')->once()->andReturn(1);
        $repository->expects('getToken')->once()->andReturn('old token');
        $repository->expects('getApiUrl')->once()->andReturn('http://example.com');

        $httpMock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    'data' => [
                        'token' => $newToken,
                        'expires_at' => $newExpiresAt->toString(),
                    ],
                ])
            ),
        ]);
        $handlerStack = HandlerStack::create($httpMock);

        $tokenRefresher = new TokenRefresher($repository, new Client(['handler' => $handlerStack]));
        $tokenRefresher->refreshToken();

        Mockery::close();

        // this made for avoiding marking this test as risky because all assertions are made by Mockery
        $this->assertTrue(true);
    }

    /**
     * @throws RefreshTokenFailedException
     */
    public function testResponseForbiddenThrowException()
    {
        $this->expectException(RefreshTokenFailedException::class);

        $repository = Mockery::mock(IntegrationSettingsRepository::class);
        $repository->expects('getIntegrationId')->once()->andReturn(1);
        $repository->expects('getToken')->once()->andReturn('old token');
        $repository->expects('getApiUrl')->once()->andReturn('http://example.com');

        $httpMock = new MockHandler([
            new Response(
                403
            ),
        ]);
        $handlerStack = HandlerStack::create($httpMock);

        $tokenRefresher = new TokenRefresher($repository, new Client(['handler' => $handlerStack]));
        $tokenRefresher->refreshToken();

        Mockery::close();
    }
}
