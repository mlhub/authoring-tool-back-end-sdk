<?php

$keys = openssl_pkey_new(['private_key_bits' => 4096, 'private_key_type' => OPENSSL_KEYTYPE_RSA]);

openssl_pkey_export($keys, $privateKey);

$keyDetails = openssl_pkey_get_details($keys);
$publicKey = $keyDetails['key'];

file_put_contents(__DIR__ . '/../keys/private.key', $privateKey);
file_put_contents(__DIR__ . '/../keys/public.key', $publicKey);

echo "keys generated successfully";
